Just a simple python script which notifies me when the road to the Gwüest lakes opens after the winter.

It uses the road status information from the power plant [Göscheneralp](https://www.kw-goeschenen.ch/aktuell/goescheneralpstrasse/) and can notify via Web Push Api with the help of [notify.run](https://notify.run/)

Just call `gwueest_notifier.py -h` for more info
