# Workflow

## Git commit convention
We follow the conventional-changelog (https://github.com/conventional-changelog/conventional-changelog) definition:

<type>(<scope>): <subject>

Allowed types:
 - **build**: a build feature
 - **ci**: a ci feature
 - **chore**: changes to auxiliary tools and libraries
 - **docs**: documentation
 - **feat**: a new feature
 - **fix**: a bug fix
 - **perf**: a performance improvement
 - **refactor**: a change that improves code quality without changing its behavior
 - **revert**: revert changes
 - **style**: a change that does not affect any meaning of the code (e.g. formatting, spaces, etc.)
 - **test**: adding/changing of tests
 