#!/usr/bin/env python3

from email.policy import default
import logging
import sys
import argparse
import requests
from bs4 import BeautifulSoup
import re
from notify_run import Notify


def handle_arguments():
    parser = argparse.ArgumentParser(
        description="Check if road to Gwüest is open and notifies via notify-run")
    parser.add_argument("-c", "--channelid",
                        help="notify-run channel id", required=True)
    parser.add_argument("-v", "--verbose", help="print more info, repeat for even more info",
                        action="count", default=0)
    global args
    args = parser.parse_args()


def set_up_logger():
    global log
    logging.basicConfig()
    log = logging.getLogger("main")
    if args.verbose == 1:
        log.setLevel(logging.INFO)
    if args.verbose > 1:
        log.setLevel(logging.DEBUG)


def grab_road_status_page():
    road_STATUS_URL = "https://www.kw-goeschenen.ch/news/goscheneralpstrasse"
    road_status_page = requests.get(road_STATUS_URL)
    if road_status_page.status_code != 200:
        log.error("Failed to get status page with error %d",
                  road_status_page.status_code)
        sys.exit(1)
    return road_status_page.text


def get_road_status_table():
    status_page = grab_road_status_page()
    soup = BeautifulSoup(status_page, "html.parser")

    name_rows = soup.find_all("p", class_=(
        "paragraph-3", "paragraph-2"))
    status_table = [[name_rows[i].text, name_rows[i+1].text]
                    for i in range(0, len(name_rows), 2)]

    status_rows = soup.find_all("p", class_=("street-status"))
    for i in range(0, len(status_rows), 2):
        if "open" in status_rows[i]["class"] and "w-condition-invisible" not in status_rows[i]["class"]:
            status_table[round(i/2)].append("offen")
        elif "open" not in status_rows[i+1]["class"] and "w-condition-invisible" not in status_rows[i+1]["class"]:
            # They removed the "closed" tag and it is only "street-status w-condition-invisible" or "street-status"
            status_table[round(i/2)].append("geschlossen")
        else:
            raise ValueError("Unknown street status")

    # Reverse table to be like it was in the past and be more in a logical order
    status_table = [value for value in reversed(status_table)]

    log.debug("Parsed status table: %s", status_table)

    # clean up all the strings from any unusual characters
    for row_index, row_value in enumerate(status_table):
        for column_index, column_value in enumerate(status_table[row_index]):
            status_table[row_index][column_index] = re.sub(
                '[^A-Za-z0-9öüä()]+', '', column_value)
    log.debug("Cleaned status table: %s", status_table)

    return status_table


def validate_status_table(status_table):
    # Just a check in case they have change something on their webpage and the script needs to be adapted accordingly
    expected_table = [["Gschenen", "Abfrutt"], ["Abfrutt", "Wiggen"], [
        "Wiggen", "Gwest"], ["Gwest", "Damm(Gscheneralp)"]]
    log.debug("Validate against table: %s", expected_table)
    for row_index, row_value in enumerate(expected_table):
        for column_index, column_value in enumerate(expected_table[row_index]):
            if column_value != status_table[row_index][column_index]:
                log.error("Table content isn't like expected: %s != %s",
                          column_value, status_table[row_index][column_index])
                sys.exit(1)
        if not bool(re.search("^(offen|geschlossen)$", status_table[row_index][2])):
            log.error("Status is neither 'offen' or 'geschlossen'")
            sys.exit(1)


def is_gwueest_road_open(status_table):
    log.debug("Checking road from %s to %s",
              status_table[2][0], status_table[2][1])
    return (status_table[2][2] == "offen")


def notify_me(text):
    CHANNEL_ID = "https://notify.run/" + args.channelid
    notify = Notify(endpoint=CHANNEL_ID)
    notify.send(text)


def main(argv):
    handle_arguments()
    set_up_logger()

    log.debug("Starting...")

    road_status_table = get_road_status_table()
    validate_status_table(road_status_table)

    if is_gwueest_road_open(road_status_table):
        log.info("Gwüest is open")
        notify_me("Time to fish, Gwüest is open")
    else:
        log.info("Gwüest is closed")
        notify_me("Gwüest is still closed")

    log.debug("...finished")


if __name__ == "__main__":
    main(sys.argv[1:])
